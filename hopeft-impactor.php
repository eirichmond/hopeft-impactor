<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://squareonemd.co.uk
 * @since             1.0.0
 * @package           Hopeft_Impactor
 *
 * @wordpress-plugin
 * Plugin Name:       Hope for Tomorrow Impactor
 * Plugin URI:        https://www.hopefortomorrow.org.uk/
 * Description:       A mirco plugin to manage Hope for Tomorrow Impact Counters.
 * Version:           1.0.0
 * Author:            Elliott Richmond
 * Author URI:        https://squareonemd.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       hopeft-impactor
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-hopeft-impactor-activator.php
 */
function activate_hopeft_impactor() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-hopeft-impactor-activator.php';
	Hopeft_Impactor_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-hopeft-impactor-deactivator.php
 */
function deactivate_hopeft_impactor() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-hopeft-impactor-deactivator.php';
	Hopeft_Impactor_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_hopeft_impactor' );
register_deactivation_hook( __FILE__, 'deactivate_hopeft_impactor' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-hopeft-impactor.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_hopeft_impactor() {

	$plugin = new Hopeft_Impactor();
	$plugin->run();

}
run_hopeft_impactor();
