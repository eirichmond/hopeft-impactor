<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://squareonemd.co.uk
 * @since      1.0.0
 *
 * @package    Hopeft_Impactor
 * @subpackage Hopeft_Impactor/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Hopeft_Impactor
 * @subpackage Hopeft_Impactor/admin
 * @author     Elliott Richmond <elliott@squareonemd.co.uk>
 */
class Hopeft_Impactor_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hopeft_Impactor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hopeft_Impactor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/hopeft-impactor-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hopeft_Impactor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hopeft_Impactor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/hopeft-impactor-admin.js', array( 'jquery' ), $this->version, false );

	}

	// Register Custom Post Type
	public function hft_register_impactor() {
	
		$labels = array(
			'name'                  => _x( 'Impactors', 'Post Type General Name', 'hopeftimpactor' ),
			'singular_name'         => _x( 'Impactor', 'Post Type Singular Name', 'hopeftimpactor' ),
			'menu_name'             => __( 'Impactors', 'hopeftimpactor' ),
			'name_admin_bar'        => __( 'Impactor', 'hopeftimpactor' ),
			'archives'              => __( 'Impactor Archives', 'hopeftimpactor' ),
			'attributes'            => __( 'Impactor Attributes', 'hopeftimpactor' ),
			'parent_item_colon'     => __( 'Parent Item:', 'hopeftimpactor' ),
			'all_items'             => __( 'All Items', 'hopeftimpactor' ),
			'add_new_item'          => __( 'Add New Item', 'hopeftimpactor' ),
			'add_new'               => __( 'Add New', 'hopeftimpactor' ),
			'new_item'              => __( 'New Item', 'hopeftimpactor' ),
			'edit_item'             => __( 'Edit Item', 'hopeftimpactor' ),
			'update_item'           => __( 'Update Item', 'hopeftimpactor' ),
			'view_item'             => __( 'View Item', 'hopeftimpactor' ),
			'view_items'            => __( 'View Items', 'hopeftimpactor' ),
			'search_items'          => __( 'Search Item', 'hopeftimpactor' ),
			'not_found'             => __( 'Not found', 'hopeftimpactor' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'hopeftimpactor' ),
			'featured_image'        => __( 'Featured Image', 'hopeftimpactor' ),
			'set_featured_image'    => __( 'Set featured image', 'hopeftimpactor' ),
			'remove_featured_image' => __( 'Remove featured image', 'hopeftimpactor' ),
			'use_featured_image'    => __( 'Use as featured image', 'hopeftimpactor' ),
			'insert_into_item'      => __( 'Insert into item', 'hopeftimpactor' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'hopeftimpactor' ),
			'items_list'            => __( 'Items list', 'hopeftimpactor' ),
			'items_list_navigation' => __( 'Items list navigation', 'hopeftimpactor' ),
			'filter_items_list'     => __( 'Filter items list', 'hopeftimpactor' ),
		);
		$args = array(
			'label'                 => __( 'Impactor', 'hopeftimpactor' ),
			'description'           => __( 'Post Type for impactor', 'hopeftimpactor' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'revisions' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 25,
			'menu_icon'             => 'dashicons-chart-bar',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'hft_impactor', $args );
	
	}
	
	public function hft_butterbean_load() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/butterbean-master/butterbean.php';
	}
	
	public function fa_icons() {
		
		$array = array(
			'f4ba' => array(
				'icon' => '<i class="fas fa-dove"></i>',
				'name' => 'dove'
			),
			'f0f9' => array(
				'icon' => '<i class="fas fa-ambulance"></i>',
				'name' => 'ambulance'
			),
			'f06b' => array(
				'icon' => '<i class="fas fa-gift"></i>',
				'name' => 'gift'
			),
			'f4be' => array(
				'icon' => '<i class="fas fa-hand-holding-heart"></i>',
				'name' => 'hand-holding-heart'
			),
			'f4c4' => array(
				'icon' => '<i class="fas fa-hands-helping"></i>',
				'name' => 'hands-helping'
			),
			'f018' => array(
				'icon' => '<i class="fas fa-road"></i>',
				'name' => 'road'
			),
			'f154' => array(
				'icon' => '<i class="fas fa-pound-sign"></i>',
				'name' => 'pound-sign'
			),
			'f4fd' => array(
				'icon' => '<i class="fas fa-user-clock"></i>',
				'name' => 'user-clock'
			),
			'f3c5' => array(
				'icon' => '<i class="fas fa-map-marker-alt"></i>',
				'name' => 'map-marker-alt'
			),
		);
		
		return $array;
		
	}
	
	public function fa_icon_choices() {
		$icons = $this->fa_icons();
		$array = array();
		foreach ($icons as $k => $v) {
			$array[$k] = $v['name'];
		}
		return $array;
	}
	

	public function hft_impact_counter( $butterbean, $post_type ) {
		
		$icons = $this->fa_icon_choices();
		
        // Register managers, sections, controls, and settings here.
		$butterbean->register_manager(
		        'example',
		        array(
		        	'label'     => esc_html__( 'Impact Counter', 'hopeft-impactor' ),
		        	'post_type' => 'hft_impactor',
		        	'context'   => 'normal',
		        	'priority'  => 'high'
		        )
		);
		
		$manager = $butterbean->get_manager( 'example' );

		$manager->register_section(
	        'data',
	        array(
	        	'label' => esc_html__( 'Data', 'hopeft-impactor' ),
				'icon'  => 'dashicons-admin-generic'
			)
		);

				$manager->register_control(
				        'impact_icon', // Same as setting name.
				        array(
				        	'type'    => 'select',
				        	'section' => 'data',
				        	'description' => esc_html__( 'Select the icon you wish to appear about the impact counter', 'hopeft-impactor' ),
				        	'label'   => esc_html__( 'Impact Icon', 'hopeft-impactor' ),
				        	'choices' => $icons,
				        	'attr'    => array( 'class' => 'widefat' )
				        )
				);

				$manager->register_control(
				        'impact_data', // Same as setting name.
				        array(
				        	'type'    => 'text',
				        	'section' => 'data',
				        	'label'   => esc_html__( 'Impact Data Number', 'hopeft-impactor' ),
				        	'attr'    => array( 'class' => 'widefat' )
				        )
				);

				$manager->register_control(
				        'impact_text', // Same as setting name.
				        array(
				        	'type'    => 'textarea',
				        	'section' => 'data',
				        	'label'   => esc_html__( 'Impact Subtext', 'hopeft-impactor' ),
				        	'attr'    => array( 'class' => 'widefat' )
				        )
				);

		$manager->register_setting(
		        'impact_icon', // Same as control name.
		        array(
		        	'sanitize_callback' => 'wp_filter_nohtml_kses'
		        )
		);

		$manager->register_setting(
		        'impact_data', // Same as control name.
		        array(
		        	'sanitize_callback' => 'wp_filter_nohtml_kses'
		        )
		);

		$manager->register_setting(
		        'impact_text', // Same as control name.
		        array(
		        	'sanitize_callback' => 'wp_filter_nohtml_kses'
		        )
		);

		$manager->register_section(
	        'colour',
	        array(
	        	'label' => esc_html__( 'Colour', 'hopeft-impactor' ),
				'icon'  => 'dashicons-art'
			)
		);

				$manager->register_control(
				        'impact_col', // Same as setting name.
				        array(
				        	'type'    => 'color',
				        	'section' => 'colour',
				        	'attr'    => array( 'class' => 'widefat' )
				        )
				);

		$manager->register_setting(
		        'impact_col', // Same as control name.
		        array(
		        	'sanitize_callback' => 'wp_filter_nohtml_kses'
		        )
		);

	}
	

}
