<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://squareonemd.co.uk
 * @since      1.0.0
 *
 * @package    Hopeft_Impactor
 * @subpackage Hopeft_Impactor/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Hopeft_Impactor
 * @subpackage Hopeft_Impactor/includes
 * @author     Elliott Richmond <elliott@squareonemd.co.uk>
 */
class Hopeft_Impactor_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'hopeft-impactor',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
