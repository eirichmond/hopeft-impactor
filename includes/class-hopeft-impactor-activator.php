<?php

/**
 * Fired during plugin activation
 *
 * @link       https://squareonemd.co.uk
 * @since      1.0.0
 *
 * @package    Hopeft_Impactor
 * @subpackage Hopeft_Impactor/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Hopeft_Impactor
 * @subpackage Hopeft_Impactor/includes
 * @author     Elliott Richmond <elliott@squareonemd.co.uk>
 */
class Hopeft_Impactor_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
