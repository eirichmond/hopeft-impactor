<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://squareonemd.co.uk
 * @since      1.0.0
 *
 * @package    Hopeft_Impactor
 * @subpackage Hopeft_Impactor/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Hopeft_Impactor
 * @subpackage Hopeft_Impactor/public
 * @author     Elliott Richmond <elliott@squareonemd.co.uk>
 */
class Hopeft_Impactor_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hopeft_Impactor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hopeft_Impactor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/hopeft-impactor-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hopeft_Impactor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hopeft_Impactor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/hopeft-impactor-public.js', array( 'jquery' ), $this->version, false );

	}
	
	public function load_fontawesome_cdn() {
		
		echo '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">';
		
	}
	
	public function display_impact_counter() {
		require plugin_dir_path( __FILE__ ) . 'partials/hopeft-impactor-public-display.php';
	}
	
	/**
	 * output the icons that are available.
	 *
	 * @since    1.0.0
	 */
	public function hft_get_impact_icon() {
		
		global $post;
		
		$adminClass = new Hopeft_Impactor_Admin('impactor', '1.0.0');
		$icons = $adminClass->fa_icons();
		
		$icon = get_post_meta( $post->ID, 'impact_icon', true );
		
		echo $icons[$icon]['icon'];
		
	}

	/**
	 * output the data.
	 *
	 * @since    1.0.0
	 */
	public function hft_get_impact_data() {
		
		global $post;
		
		$impact_data = get_post_meta( $post->ID, 'impact_data', true );
		
		echo esc_html( $impact_data );
		
	}
	
	/**
	 * output the data.
	 *
	 * @since    1.0.0
	 */
	public function hft_get_impact_subtext() {
		global $post;
		
		$impact_subtext = get_post_meta( $post->ID, 'impact_text', true );
		
		echo esc_html( $impact_subtext );
	}
	
	/**
	 * output the data.
	 *
	 * @since    1.0.0
	 */
	public function hft_impact_col() {
		global $post;
		
		$impact_colour = get_post_meta( $post->ID, 'impact_col', true );
		
		echo esc_html( 'style=color:'.$impact_colour );
	}
	
	
	
}
