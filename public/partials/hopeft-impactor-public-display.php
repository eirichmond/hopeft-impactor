<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://squareonemd.co.uk
 * @since      1.0.0
 *
 * @package    Hopeft_Impactor
 * @subpackage Hopeft_Impactor/public/partials
 */
 
$args = array(
	'post_type' => 'hft_impactor',
	'posts_per_page' => -1,
	'post_status' => 'publish'
);
 
// the query
$impactors = new WP_Query( $args );

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="impact-counter">
	<div class="row">
		
		<?php if ( $impactors->have_posts() ) : while ( $impactors->have_posts() ) : $impactors->the_post(); ?>
		
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<div class="impact-icon" <?php $this->hft_impact_col(); ?>><?php $this->hft_get_impact_icon(); ?></div>
				
				<div class="impact-number" <?php $this->hft_impact_col(); ?>><?php $this->hft_get_impact_data(); ?></div>
				
				<div class="impact-subtext"><?php $this->hft_get_impact_subtext(); ?></div>
			
			
			</article>

			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		
		<?php endif; ?>		
		
	
   	</div>
</div>

